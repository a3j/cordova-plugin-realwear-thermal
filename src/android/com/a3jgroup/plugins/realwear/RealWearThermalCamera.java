package com.a3jgroup.plugins.realwear;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

public class RealWearThermalCamera extends CordovaPlugin {

    public static final String INTENT_IMAGE_CAPTURE = "com.realwear.thermalcamera.IMAGE_CAPTURE";
    public static final int INTENT_REQUEST_CODE = 123;
    private CallbackContext callbackContext;

    private static final String RETURN_IMAGE = "image";
    private static final String RETURN_SCALE = "scale";
    private static final String RETURN_SPOT_TEMP = "captured_temp";
    private static final String RETURN_MIN_TEMP = "min_temp";
    private static final String RETURN_MAX_TEMP = "max_temp";
    private static final String RETURN_CANCELLED = "cancelled";
    private static final String LOGGER_PREFIX = "RealWearThermal";

    /**
     * Constructor.
     */
    public RealWearThermalCamera() {
    }

    /**
     * Executes the request.
     *
     * This method is called from the WebView thread.
     *
     * @param action          The action to execute.
     * @param args            The exec() arguments.
     * @param callbackContext The callback context used when calling back into JavaScript.
     * @return                Whether the action was valid.
     *
     * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        this.callbackContext = callbackContext;

        final CordovaPlugin that = this;

        cordova.getThreadPool().execute(new Runnable() {
            public void run() {

                Intent intent = new Intent(INTENT_IMAGE_CAPTURE);
                // start the activity
                that.cordova.startActivityForResult(that, intent, INTENT_REQUEST_CODE);
            }
        });

        return true;
    }

    /**
     * Called when the thermal intent completes.
     *
     * @param requestCode The request code originally supplied to startActivityForResult(),
     *                       allowing you to identify who this result came from.
     * @param resultCode  The integer result code returned by the child activity through its setResult().
     * @param intent      An Intent, which can return result data to the caller (various data can be attached to Intent "extras").
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == INTENT_REQUEST_CODE && this.callbackContext != null) {
            if (resultCode == Activity.RESULT_OK) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put(RETURN_IMAGE, intent.getParcelableExtra("data"));
                    obj.put(RETURN_SCALE, intent.getParcelableExtra("scale"));
                    obj.put(RETURN_SPOT_TEMP, intent.getStringExtra("captured_temp"));
                    obj.put(RETURN_MIN_TEMP, intent.getStringExtra("min_temp"));
                    obj.put(RETURN_MAX_TEMP, intent.getStringExtra("max_temp"));
                    obj.put(RETURN_CANCELLED, false);
                } catch (JSONException e) {
                    Log.d(LOGGER_PREFIX, "This should never happen");
                }
                // Return successfull callback
                this.callbackContext.success(obj);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put(RETURN_IMAGE, "");
                    obj.put(RETURN_SCALE, "");
                    obj.put(RETURN_SPOT_TEMP, "");
                    obj.put(RETURN_MIN_TEMP, "");
                    obj.put(RETURN_MAX_TEMP, "");
                    obj.put(RETURN_CANCELLED, true);
                } catch (JSONException e) {
                    Log.d(LOGGER_PREFIX, "This should never happen");
                }
                // User cancelled the interaction
                this.callbackContext.success(obj);
            } else {
                // Unexpected error
                this.callbackContext.error("Unexpected error");
            }
        }
    }

}