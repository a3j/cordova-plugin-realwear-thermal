var exec = cordova.require("cordova/exec");

var scanInProgress = false;

/**
 * Constructor.
 *
 * @returns {RealWearThermalCamera}
 */
function RealWearThermalCamera() {}

/**
 * Read temperature from thermal camera.
 *
 * @param {Function} successCallback This function will recieve a result object: {
 *        image : ''  // Image bitmap from the camera
 *        scale : ''  // Scale factor from the image
 *        captured_temp : '72'  // Spot check temp
 *        min_temp : '72'  // Min temp in view
 *        max_temp : '72'  // Max temp in view
 *        cancelled : true/false, // Was canceled.
 *    }
 * @param {Function} errorCallback
 * @param config
 */
RealWearThermalCamera.prototype.scan = function (successCallback, errorCallback, config) {

    if (config instanceof Array) {
        // do nothing
    } else {
        if (typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function () {};
    }

    if (typeof errorCallback != "function") {
        return;
    }

    if (typeof successCallback != "function") {
        return;
    }

    if (scanInProgress) {
        errorCallback('Scan is already in progress');
        return;
    }

    scanInProgress = true;

    exec(
        function(result) {
            scanInProgress = false;
            successCallback(result);
        },
        function(error) {
            scanInProgress = false;
            errorCallback(error);
        },
        'RealWearThermalCamera',
        'scan',
        config
    );
};

var rwThermalCamera = new RealWearThermalCamera();
module.exports = rwThermalCamera;
