# RealWear Thermal Camera
This plugin works with the RealWear Thermal Camera app, v1.4.

To install the plugin, run the following command within the project:
`cordova plugin add https://gitlab.com/a3j/cordova-plugin-realwear-thermal --save`

## Sample code to use the plugin
`if (window.cordova) {
	window.cordova.plugins.realWearThermalCamera
		.scan(data => {
			console.log("data", data);
		}, error => {
			console.log("error", error);
		}, []);
} else {
	console.log("Cordova not available");
}`
